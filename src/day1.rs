use std::{fs::File, io::Read};

pub fn sonar_sweep() -> i32 {
    compute_depth_measurement_increases(parse_input())
}

pub fn improved_sonar_sweep() -> i32 {
    let sliding_windows = group_by_sliding_windows();
    compute_depth_measurement_increases(sliding_windows)
}

fn compute_depth_measurement_increases(xs: Vec<i32>) -> i32 {
    let mut previous_depth = None;

    xs.iter().fold(0, |acc, x| {
        let mut res = acc;
        if let Some(p) = previous_depth {
            if p < x {
                res += 1;
            }
        }
        previous_depth = Some(x);
        res
    })
}

fn group_by_sliding_windows() -> Vec<i32> {
    let xs = parse_input();
    let mut res = Vec::new();

    for i in 1..xs.len() - 1 {
        let a = xs[i - 1];
        let b = xs[i];
        let c = xs[i + 1];
        res.push(a + b + c);
    }

    res
}

fn parse_input() -> Vec<i32> {
    let mut input_file = File::open("data/day1").unwrap();
    let mut input = String::new();
    input_file.read_to_string(&mut input).unwrap();
    input.lines().map(|l| l.parse::<i32>().unwrap()).collect()
}
