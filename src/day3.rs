use std::{fs::File, io::Read};

const NB_REPORT_VALUE_DIGITS: usize = 12;

pub fn power_consumption() -> u32 {
	let gamma = compute_gamma_rate();
	let epsilon = compute_epsilon_rate(&gamma);
	u32::from_str_radix(&gamma, 2).unwrap() * epsilon
}

fn compute_gamma_rate() -> String {
	let report = parse_input();

	let mut gamma_rate = String::new();
	
    for i in 0..NB_REPORT_VALUE_DIGITS {
        let mut nb_of_ones = 0;

        for j in 0..report.len() {
            if report[j].as_bytes()[i] as char == '1' {
                nb_of_ones += 1;
            }
        }

        if nb_of_ones >= report.len() / 2 {
            gamma_rate.push('1');
        }
        else {
            gamma_rate.push('0');
        };
    }

	gamma_rate
}

fn compute_epsilon_rate(gamma: &str) -> u32 {
    let mut epsilon = String::new();
    gamma.chars().for_each(|c| {
        match c {
            '0' => epsilon.push('1'),
            '1' => epsilon.push('0'),
            _ => panic!(),
        };
    });
    u32::from_str_radix(&epsilon, 2).unwrap()
}

fn parse_input() -> Vec<String> {
    let mut input_file = File::open("data/day3").unwrap();
    let mut input = String::new();
    input_file.read_to_string(&mut input).unwrap();
    input.lines().map(|l| l.to_owned()).collect()
}
