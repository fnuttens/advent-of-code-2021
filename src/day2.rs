use std::{fs::File, io::Read};

enum SubmarineCommand {
    Forward(u32),
    Down(u32),
    Up(u32),
}

pub fn compute_position() -> u32 {
    let mut aim = 0;

    let final_position = parse_commands().iter().fold((0, 0), |acc, x| match x {
        SubmarineCommand::Forward(u) => (acc.0 + u, acc.1 + aim * u),
        SubmarineCommand::Down(u) => {
            aim += u;
            acc
        }
        SubmarineCommand::Up(u) => {
            aim -= u;
            acc
        }
    });

    final_position.0 * final_position.1
}

fn parse_commands() -> Vec<SubmarineCommand> {
    let mut input_file = File::open("data/day2").unwrap();
    let mut input = String::new();
    input_file.read_to_string(&mut input).unwrap();
    input
        .lines()
        .map(|l| {
            let unit_char = l.as_bytes()[l.len() - 1] as char;
            let units = unit_char.to_digit(10).unwrap();

            match l.as_bytes()[0] as char {
                'f' => SubmarineCommand::Forward(units),
                'd' => SubmarineCommand::Down(units),
                'u' => SubmarineCommand::Up(units),
                _ => panic!(),
            }
        })
        .collect()
}
